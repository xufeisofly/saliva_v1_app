Rails.application.routes.draw do
  get 'steps/new'

  resources :orders do
    member do
      get 'pay'
      get 'charge'
    end
  end
  resources :line_items
  resources :carts do
    collection do
      delete :destroy_all
    end
  end
  root 'saliva_pages#home'
  get 'about' => 'saliva_pages#about'
  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  resources :users do
    member do
      get 'favorite'
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :dishes do
    collection do
      get :search
    end
    member do
      get :following, :followers
    end
  end
  resources :relationships, only: [:create, :destroy]
end
