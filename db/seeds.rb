# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.create!(name: "Example User", email: "example@railstutorial.org", password: "foobar", password_confirmation: "foobar", admin: true, activated: true, activated_at: Time.zone.now)
User.create!(name: "sofly", email: "849751669@qq.com", password: "alohomora7", password_confirmation: "alohomora7", admin: true, activated: true, activated_at: Time.zone.now)
31.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name, email: email, password: password, password_confirmation: password, activated: true, activated_at: Time.zone.now)
end

=begin
100.times do |n|
  name = "大山炸猪排#{n+1}"
  description = "#{n+1}描述"
  price = 0.1
  bannerimg = "bannerimg"
  Dish.create!(name: name, description: description, price: price, bannerimg: bannerimg)
end
Dish.create!(name: "寿司", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "刀削面", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "炸猪排", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "卜卜星", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "沙拉", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "上好佳", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "小浣熊", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "木须肉", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "海王类", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "西红柿炒鸡蛋", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
Dish.create!(name: "巨无霸", step1: "西红柿洗净切块", step2: "鸡蛋打碎，放入少许盐", step3: "开火，放入葱花煸炒", step4: "待油温至70度左右后放入鸡蛋煸炒", step5: "鸡蛋炒至金黄色，放入切好的西红柿", step6: "最后放入一勺盐，煸炒出锅", description: "纯手工制作，鲜嫩多汁……", price: 19.9)
=end
#user.follow(dish)

Order.transaction do
  (1..40).each do |i|
    Order.create(name: "Customer #{i}", address: "北京市", address2: "海淀区", address3: "#{i} Main Street", phone: "1322222#{i}")
  end
end
