class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.integer :step_id
      t.text :content
      t.string :stepimg

      t.timestamps null: false
    end
  end
end
