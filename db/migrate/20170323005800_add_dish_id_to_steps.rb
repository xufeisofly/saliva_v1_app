class AddDishIdToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :dish_id, :integer
  end
end
