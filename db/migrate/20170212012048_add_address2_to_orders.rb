class AddAddress2ToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :address2, :text
    add_column :orders, :address3, :text
  end
end
