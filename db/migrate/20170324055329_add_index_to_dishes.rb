class AddIndexToDishes < ActiveRecord::Migration
  def change
    add_reference :dishes, :user, index: true, foreign_key: true
    add_index :dishes, [:user_id, :created_at]
  end
end
