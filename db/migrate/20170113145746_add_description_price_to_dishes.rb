class AddDescriptionPriceToDishes < ActiveRecord::Migration
  def change
    add_column :dishes, :description, :text
    add_column :dishes, :price, :decimal, precision: 8, scale: 2
  end
end
