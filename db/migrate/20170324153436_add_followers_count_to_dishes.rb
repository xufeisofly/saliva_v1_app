class AddFollowersCountToDishes < ActiveRecord::Migration
  def change
    add_column :dishes, :followers_count, :integer, default: 0
  end
end
