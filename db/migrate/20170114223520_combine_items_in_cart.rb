class CombineItemsInCart < ActiveRecord::Migration
  def change
    Cart.all.each do |cart|
      sums = cart.line_items.group(:dish_id).sum(:quantity)

      sums.each do |dish_id, quantity|
        if quantity > 1
          #remove individual items
          cart.line_items.where(dish_id: dish_id).delete_all

          #replace with a single item
          cart.line_items.create(dish_id: dish_id, quantity: quantity)
        end
      end
    end
  end
end
