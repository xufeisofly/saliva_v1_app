class RemoveStepsFromDishes < ActiveRecord::Migration
  def change
    remove_column :dishes, :step1
    remove_column :dishes, :step2
    remove_column :dishes, :step3
    remove_column :dishes, :step4
    remove_column :dishes, :step5
    remove_column :dishes, :step6
    remove_column :dishes, :step7
    remove_column :dishes, :step8
    remove_column :dishes, :step9
    remove_column :dishes, :step10
    remove_column :dishes, :step11
    remove_column :dishes, :step12
    remove_column :dishes, :step13
    remove_column :dishes, :step14
    remove_column :dishes, :step15
    remove_column :dishes, :step16
    remove_column :dishes, :step17
    remove_column :dishes, :step18
    remove_column :dishes, :step19
    remove_column :dishes, :step20
  end
end
