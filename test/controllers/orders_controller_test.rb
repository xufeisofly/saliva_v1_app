require 'test_helper'

class OrdersControllerTest < ActionController::TestCase
  setup do
    @order = orders(:one)
    @admin = users(:michael)
  end

  test "requires item in cart" do
    get :new
    assert_redirected_to root_path
    assert_equal flash[:notice], '你的购物车是空的'
  end

  test "should get index" do
    log_in_as(@admin)
    get :index
    assert_response :success
    assert_not_nil assigns(:orders)
  end

  test "should get new" do
    cart = Cart.create
    session[:cart_id] = cart.id
    LineItem.create(cart: cart, dish: dishes(:pizza))
    get :new
#    assert_response :false
  end

  test "should create order" do
    assert_difference('Order.count') do
      post :create, order: { address: @order.address, address2: @order.address2, address3: @order.address3, name: @order.name, phone: @order.phone }
    end

    assert_redirected_to pay_order_path(assigns(:order))
  end

  test "should show order" do
    get :show, id: @order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @order
    assert_response :success
  end

  test "should update order" do
    patch :update, id: @order, order: { address: @order.address, address2: @order.address2, address3: @order.address3, name: @order.name, phone: @order.phone }
    assert_redirected_to order_path(assigns(:order))
  end

  test "should destroy order" do
    log_in_as(@admin)
    assert_difference('Order.count', -1) do
      delete :destroy, id: @order
    end

    assert_redirected_to orders_path
  end
  #对PING++付款是否返回charge的JSON进行验证 
#  test "should purchase order" do
#    get :pay, id: @order
#    assert_response :success
#    get :charge, id:@order
#    charge = JSON.parse(@response.body)
#    assert_equal "charge", charge["object"]
#  end
end
