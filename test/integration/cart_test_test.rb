require 'test_helper'

class CartTestTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @dish = dishes(:pizza)
  end
  # 测试show页面的加入购物车功能，Ajax
  test "add dish to the cart via Ajax" do
    assert_difference '@dish.line_items.count', 1 do
      xhr :post, line_items_path, dish_id: @dish.id
    end
  end
end
