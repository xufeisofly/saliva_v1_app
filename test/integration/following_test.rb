require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:michael)
    @dish = dishes(:pizza)
    log_in_as(@user)
  end

  test "following part in user favorite page" do
    get favorite_user_path(@user)
    assert_not @user.following.empty?
    assert_match @user.following.count.to_s, response.body
    @user.following.each do |dish|
      assert_select "a[href=?]", dish_path(dish)
    end
  end

  test "should follow a dish with Ajax" do
    assert_difference '@user.following.count', 1 do
      xhr :post, relationships_path, followed_id: @dish.id
    end
  end

  test "should unfollow a dish with Ajax" do
    @user.follow(@dish)
    relationship = @user.active_relationships.find_by(followed_id: @dish.id)
    assert_difference '@user.following.count', -1 do
      xhr :delete, relationship_path(relationship)
    end
  end
end
