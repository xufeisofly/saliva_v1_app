require 'test_helper'

class DishesIndexTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @dish = dishes(:pizza)
    @admin = users(:michael)
    @non_admin = users(:archer)
  end

  test "dishes index" do
    get dishes_path
    assert_template 'dishes/index'
    dishes = Dish.all
    dishes.each do |dish|
      assert_select 'a[href=?]', dish_path(dish)
    end
  end

  test "dishes new" do
    get new_dish_path
    assert_redirected_to login_path
    log_in_as(@admin)
    get new_dish_path
    #无效提交
    assert_no_difference 'Dish.count' do
      post dishes_path, dish: {name: "", description: "", bannerimg: "", price: 0}
    end
    #有效提交
    bannerimg = fixture_file_upload('test/fixtures/dish_1.jpg', 'image/jpg')
    stepimg = fixture_file_upload('test/fixtures/dish_1.jpg', 'image/jpg')
    assert_difference 'Dish.count', 1 do
      post dishes_path, dish: {name: "mm", description: "delicious", price: 9.9, bannerimg: bannerimg, step: { content: "step1", stepimg: stepimg }}
    end
  end

  test "index as admin including delete links" do
    log_in_as(@admin)
    get dishes_path
    assert_template 'dishes/index'
    Dish.all.each do |dish|
      assert_select 'a[href=?]', dish_path(dish), text: 'delete', method: :delete
    end
  end

  test "index as non_admin" do
    log_in_as(@non_admin)
    get dishes_path
    assert_select 'a', text: 'delete', count: 0
  end

  test "dishes destroy" do
    log_in_as(@admin)
    get dishes_path
    assert_difference 'Dish.count', -1 do
      delete dish_path(@dish)
    end
  end
end
