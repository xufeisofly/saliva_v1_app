class ApplicationMailer < ActionMailer::Base
  default from: "SalivaGroup@saliva.com"
  layout 'mailer'
end
