class Step < ActiveRecord::Base
  belongs_to :dish
  mount_uploader :stepimg, StepUploader
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  validates :content, presence: true, length: {maximum: 50}
  validates :stepimg, presence: true
  validate :picture_size
    
  private

    def picture_size
      if stepimg.size > 1.megabytes
        errors.add(:stepimg, "should be less than 1MB")
      end
    end
end
