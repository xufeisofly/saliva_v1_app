require 'elasticsearch/model'
class Dish < ActiveRecord::Base
  belongs_to :user
  has_many :passive_relationships, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :line_items, dependent: :destroy
  has_many :steps, dependent: :destroy, foreign_key: "dish_id"
  accepts_nested_attributes_for :steps, reject_if: lambda { |a| a[:content].blank? }, allow_destroy: true
  before_destroy :ensure_not_referenced_by_any_line_item

  mount_uploader :bannerimg, PictureUploader
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h
  after_update :crop_img

  validates :name, presence: true, length: {maximum: 10}
  validates :description, presence: true
#  validates :price, presence: true, numericality: {greater_than_or_equal_to: 0.01}
  validates :bannerimg, presence: true
  validate :picture_size
  include Elasticsearch::Model
#  include Elasticsearch::Model::Callbacks
  #使用elasticsearch自动callback会概率性导致Elasticsearch::Transport::Transport::Errors:,因此改用下面的custom
  #callback
  after_commit on: [:create] do
      __elasticsearch__.index_document
  end

  after_commit on: [:update] do
      __elasticsearch__.index_document
  end

  after_commit on: [:destroy] do
      __elasticsearch__.delete_document
  end 
#################################################################

  def followed_by?(user)
    followers.include?(user)
  end

  def crop_img
    bannerimg.recreate_versions! if crop_x.present?
  end

  def followers_counting(dish)
    update_attribute(followers_count: dish.followers.count)
  end

  private

    def ensure_not_referenced_by_any_line_item
      if line_items.empty?
        return true
      else
        errors.add(:base, 'Line Items present')
        return false
      end
    end

    def picture_size
      if bannerimg.size > 1.megabytes
        errors.add(:bannerimg, "should be less than 1MB")
      end
    end

end
Dish.import force: true
