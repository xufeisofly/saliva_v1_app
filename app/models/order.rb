require 'pingpp'
class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy
  belongs_to :user
  PAYMENT_TYPES = ["北京市"]
  PAYMENT_TYPES2 = ["海淀区","朝阳区"]

  #validates :address, inclusion: PAYMENT_TYPES
  #validates :address2, inclusion: PAYMENT_TYPES2
  #validates :name, :address3, presence: true
  #validates :phone, presence: true

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end
  
  def total_price
    line_items.to_a.sum {|item| item.total_price}
  end

  def pay_url
    #获取api_key以及app_id
    Pingpp.api_key = "sk_test_iLCK0O4qLaLK5e5O4C4mnLiP"
    app_id = "app_0iLejH0y5SaLHK4C"
    #不同支付渠道的回调地址
    #ping++平台新建一个订单
    begin
      charge = Pingpp::Charge.create(
        order_no: order_no,
        app: {id: app_id},
        channel: "alipay_pc_direct",
        amount: 10,
        client_ip: "10.129.204.83",
        currency: "cny",
        subject: "saliva purchase",
        body: "My body",
        extra: {success_url: "http://www.salivagroup.cn/orders/#{id}"}
      )

      return charge
    rescue Pingpp::PingppError => error
      logger.error 'ping++平台创建订单失败'
      logger.error error.http_body
      return false
    end
  end
end
