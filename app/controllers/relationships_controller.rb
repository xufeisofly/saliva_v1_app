class RelationshipsController < ApplicationController
  before_action :logged_in_user

  def create
    @dish = Dish.find(params[:followed_id])
    current_user.follow(@dish)
    respond_to do |format|
      format.html {redirect_to @dish}
      format.js
    end
  end

  def destroy
    @dish = Relationship.find(params[:id]).followed
    current_user.unfollow(@dish)
    respond_to do |format|
      format.html {redirect_to @dish}
      format.js
    end
  end
end
