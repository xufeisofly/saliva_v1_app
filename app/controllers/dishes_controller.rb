class DishesController < ApplicationController
  before_action :logged_in_user, only: [:new, :destroy, :edit, :update]
  before_action :set_dish, only: [:edit, :update, :following, :followers, :destroy]
  before_action :correct_user_of_dish, only: [:edit, :update]

  def followimg

  end

  def followers

  end
 
  def show
    @cart = current_cart
    begin
      @dish = Dish.find(params[:id])
     # @line_item = @cart.add_dish(@dish.id)
    rescue ActiveRecord::RecordNotFound
      logger.error "Attempt to access invalid dish #{params[:id]}"
      redirect_to dishes_url, notice: '您找的菜品不存在...'
    else
      respond_to do |format|
        format.html
        format.xml {render :xml => @dish}
      end
    end
  end

  def index
    #@dishes = Dish.joins(:passive_relationships).group("dishes.id").order("count(followed_id)").page(params[:page])
    #@dishes = Dish.order(:created_at).page(params[:page])
    @dishes = Kaminari.paginate_array(Dish.all.sort_by{|o| o.followers.count}.reverse).page(params[:page]).per(20)
    @cart = current_cart
  end

  def search
    if params[:query].nil?
      @dishes = Kaminari.paginate_array([]).page(params[:page])
    else
      @dishes = Dish.search(params[:query].split.join('AND')).page(params[:page]).records
    end
    respond_to do |format|
      format.html {render :action => "index"}
      format.xml {render :xml => @dishes}
    end
  end

  def new
    @dish = Dish.new
    3.times { @dish.steps.build }
  end

  def edit
  end

  def create
#    @dish = Dish.new(dish_params)
    @dish = current_user.dishes.build(dish_params)
    # 把价格默认设为1
    @dish.price = 1
    if @dish.save
      if params[:dish][:bannerimg].present?
        begin
          #render :crop
          redirect_to @dish
        rescue
          redirect_to @dish
        end
      else
        redirect_to @dish
      end
    else
      render :new
    end
  end

  def update
    if @dish.update_attributes(dish_params)
      if params[:dish][:bannerimg].present?
        #render :crop
        redirect_to @dish
      else
        redirect_to @dish
      end
    else
      render :new
    end
  end

  def destroy
    @dish.destroy
    respond_to do |format|
      format.html { redirect_to dishes_url, notice: 'dish was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_dish
      @dish = Dish.find(params[:id])
    end  


    def dish_params
      params.require(:dish).permit(:name, :description, :price, :bannerimg, :crop_x, :crop_y, :crop_w, :crop_h, :followers_count, steps_attributes: [:id, :content, :stepimg, :_destroy])
    end

    def correct_user_of_dish
      @dish = Dish.find(params[:id])
      redirect_to root_url unless current_user?(@dish.user)
    end

    def followers_counting
    end
end
